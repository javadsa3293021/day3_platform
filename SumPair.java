import java.io.*;
import java.util.*;

class GFG {

    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int tc = Integer.parseInt(br.readLine().trim());
        while (tc-- > 0) {
            String[] inputLine;
            inputLine = br.readLine().trim().split(" ");
            int n = Integer.parseInt(inputLine[0]);
            int k = Integer.parseInt(inputLine[1]);
            int[] arr = new int[n];
            inputLine = br.readLine().trim().split(" ");
            for (int i = 0; i < n; i++) {
                arr[i] = Integer.parseInt(inputLine[i]);
            }
            int ans = new Solution().getPairsCount(arr, n, k);
            System.out.println(ans);
        }
    }
}

class Solution {
    static int getPairsCount(int[] arr, int n, int k) {
        HashMap<Integer, Integer> countMap = new HashMap<>();

        for (int i = 0; i < n; i++) {
            countMap.put(arr[i], countMap.getOrDefault(arr[i], 0) + 1);
        }

        int pairCount = 0;

        for (int i = 0; i < n; i++) {
            int complement = k - arr[i];
            if (countMap.containsKey(complement)) {
                pairCount += countMap.get(complement);

                if (arr[i] == complement) {
                    pairCount--;
                }
            }
        }

        return pairCount / 2;
    }
}

